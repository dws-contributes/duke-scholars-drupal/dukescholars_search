import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Scholar from "./Components/Scholar";
import defaultThumbnail from './images/not-found.jpeg';
import SearchForm from './Components/SearchForm';
class ScholarsSearch extends Component {

    constructor(props) {

        super(props);

        this.state = {
            searchString: '',
            scholars: [],
            response: null,
            error: false
        };

        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }

    componentDidCatch(error, info) {
        this.setState({error: error})
    }

    handleChange(searchString) {
        // scrub search string? wait for a certain number of characters?
        if (searchString.length > 3) {

            let search = '"' + searchString.replace(" ", " AND ") + '"';

            this.setState({searchString: search} );
            this.search(search);

        } else if (searchString.length === 0) {

            this.setState({scholars: [], searchString: ''})

        }
    }

    componentDidMount() {
        if (this.props.apiKey.length < 30) {
            this.setState({error: 'Please set an API Key'})
        }
    }

    processItems(data) {

        let processedScholars = [];

        let normalizeName = this.normalizeName;

        const {response, highlighting} = data;

        for (let i = 0; i < response.docs.length; i++) {

            const hightlightKey = Object.keys(highlighting)[i];

            processedScholars.push({
                "uri": response.docs[i].URI,
                "name": normalizeName(response.docs[i].nameRaw[0]),
                "thumbnailUrl": (response.docs[i].THUMBNAIL_URL.length > 0) ? response.docs[i].THUMBNAIL_URL : defaultThumbnail,
                "prefferedTitle": response.docs[i].PREFERRED_TITLE[0],
                "highlight": highlighting[hightlightKey]['duke_text']
            });
        }

        return processedScholars;
    }

    normalizeName(name) {
        var nameParts = name.split(', ');
        return nameParts[1] + " " + nameParts[0];
    }

    search(searchString) {

        if (!this.props.apiKey) {
            throw new Error('no search api key found');
        }

        const params = {
            'apikey': this.props.apiKey,
            'q': searchString,
            'wt': 'json',
            'fq': 'type:(*Person)',
            'fl': 'PREFERRED_TITLE, nameRaw, THUMBNAIL_URL, public_image_text, URI',
            'hl': true,
            'hl.fragsize': 175,
            'hl.usePhraseHighlighter': true,
            'hl.fl': 'duke_text',
            'mm': true,
            'qf': 'duke_text duke_text_unstemmed nameText^2.0 nameUnstemmed^2.0 nameStemmed^2.0 nameLowercase',
            'rows': '10'
        };

        let esc = encodeURIComponent;

        let queryString = Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');

        fetch("https://scholars-search.api.oit.duke.edu?" + queryString, {
            method: "GET",
            cache: "no-cache",
            mode: "cors",
            redirect: "follow",
            referrer: "no-referrer",
        })
            .then(res => res.json())
            .then(res => this.processItems(res))
            .then(res => { this.setState({scholars: res} ) })
        .catch(function(error) {
            console.error(`Fetch error =\n`, error);
        });
    };

    render() {

        let results = '';

        if (this.state.error) {
            results = <h1>there was an error</h1>
        } else if (this.state.scholars.length > 0) {
            results = this.state.scholars.map(scholar => <Scholar
                uri={scholar.uri}
                key={scholar.uri}
                thumbnailUrl={scholar.thumbnailUrl}
                name={scholar.name}
                preferredTitle={scholar.prefferedTitle}
                highlight={scholar.highlight}/>
            )
        } else {
            results = <div>use the form above to search Scholars@Duke</div>
        }

        return (
            <section>
                <header>
                    <h1 className="App-title">{this.props.title}</h1>
                </header>
                <SearchForm handleChange={this.handleChange} />
                {results}
            </section>
        );
    }
}

ScholarsSearch.propTypes = {
    searchString: PropTypes.string,
    scholars: PropTypes.array,
    response: PropTypes.object
};

export default ScholarsSearch;
