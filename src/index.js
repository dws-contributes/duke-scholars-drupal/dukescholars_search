import React from 'react';
import ReactDOM from 'react-dom';
import ScholarsSearch from './App';

const container = document.getElementById('scholarsSearch');
ReactDOM.render(<ScholarsSearch apiKey={container.getAttribute('apiKey')} title={container.getAttribute('appTitle')} />, container);
