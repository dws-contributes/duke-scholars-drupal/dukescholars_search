import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
   root: {
       display: 'flex'
   }
});

class SearchForm extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            searchString: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.clearForm = this.clearForm.bind(this);
    }

    handleChange(e) {
        this.setState({ searchString: e.target.value });
        this.props.handleChange(e.target.value);
    }

    clearForm() {
        this.setState({searchString: ''})
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <div>
                    <IconButton disabled="disabled">
                        <SearchIcon/>
                    </IconButton>
                </div>
            <FormControl fullWidth={true}>
                <TextField
                 id="search-scholars"
                 label="Search Scholars@Duke"
                 value={this.state.searchString}
                 onChange={this.handleChange}
                 variant="outlined"
                >

                </TextField>
            </FormControl>
                <div>
                    <IconButton onClick={this.clearForm}>
                        <ClearIcon />
                    </IconButton>
                </div>
            </div>
        )
    }
}

SearchForm.propTypes = {
    handleChange: PropTypes.func.isRequired
};

export default withStyles(styles)(SearchForm);