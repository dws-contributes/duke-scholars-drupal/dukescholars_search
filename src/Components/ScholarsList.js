import React from 'react';
import PropTypes from 'prop-types';
import Scholar from './Scholar';
import List from '@material-ui/core/List';

function ScholarList(props) {

    return (
        <div>
            <List>
                {this.props.scholars.map(scholar => <Scholar 
                    uri={scholar.uri}
                    key={scholar.uri}
                    thumbnailUrl={scholar.thumbnailUrl}
                    name={scholar.name}
                    preferredTitle={scholar.prefferedTitle}
                />)}            
            </List>
        </div>
    )
}

ScholarList.propTypes = {
    scholars: PropTypes.array.isRequired
}

export default ScholarList;