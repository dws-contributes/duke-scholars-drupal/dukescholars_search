import React, { Component } from 'react';
import styled from 'styled-components';
import propTypes from "prop-types";

const ScholarWrapper = styled.div`
    display: grid;
    grid-template-columns: 64px 1fr;
    background-color: #ffffff;
    padding: 10px 0;
    margin: 0 10px;
    border-bottom: 2px solid #e6e6e6;
    cursor: pointer;
    `;

const ScholarThumb = styled.div`
    height: 54px;
    width: 54px;
    background-size: cover;
    border-radius: 30px;
    `;

const ScholarName = styled.span`
    color: rgb(6, 128, 205);
    display: block;
    font-size: 14px;
    width: 100%;
    font-weight: 700;
    min-height: 20px;
    text-overflow: ellipsis;
    overflow: hidden;
    margin: 5px 0 0;
    `;

const ScholarPreferredTitle = styled.span`
    font-size: 12px;
    color: rgb(84, 84, 84);
    line-height: 1.15;
    font-weight: 400;
    font-style: italic;
    display: block;
    width: 100%;
    `;

const ScholarHighlight = styled.div`
    font-size: 12px;
`;

class Scholar extends Component {

    constructor(props) {
        super(props);
        this.handleClick.bind(this);
    }

    componentDidMount() {

    }

    handleClick(uri) {
        window.open(uri);
    }

    renderMarkup(value) {
        return {__html: value}
    }

    render() {

        return(
            <ScholarWrapper onClick={() => this.handleClick(this.props.uri)}>
                <ScholarThumb style={{backgroundImage: "url(" + this.props.thumbnailUrl + ")"}} />
                <div>
                    <ScholarName>{this.props.name}</ScholarName>
                    <ScholarPreferredTitle>{this.props.preferredTitle}</ScholarPreferredTitle>
                    <ScholarHighlight dangerouslySetInnerHTML={this.renderMarkup(this.props.highlight)}></ScholarHighlight>
                </div>
            </ScholarWrapper>
        )

    }
}

export default Scholar;

Scholar.propTypes = {
    uri: propTypes.string,
    thumbnailUrl: propTypes.string,
    preferredTitle: propTypes.string,
    name: propTypes.string,
    highlight: propTypes.string
};

Scholar.defaultProps = {
    uri: null,
    thumbnailUrl: null,
    preferredTitle: '',
    name: '',
    highlight: ''
};