import React from 'react';
import styled from 'styled-components';

const ErrorWrapper = styled.div`
    border: 1px solid red;
    border-radius: 8px;
    background-color: #ffdede;
    color: black;
    padding: 10px;
    margin-top: 10px;
`;

function ErrorMessage(props) {
    return <ErrorWrapper>{props.message}</ErrorWrapper>
}

export default ErrorMessage;